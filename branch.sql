CREATE TABLE branch (
    branch_id int NOT NULL ,
    branch_addr varchar(255),
    ISBN varchar(255) NOT NULL,
    title varchar(255) NOT NULL,
    author varchar(255),
    publisher varchar(255),
    num_copies int,
    PRIMARY KEY (branch_id)
    );

