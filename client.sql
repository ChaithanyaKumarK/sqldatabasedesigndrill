CREATE TABLE staff (
    staff_id int NOT NULL ,
    staff_name varchar(255) NOT NULL,
    staff_location varchar(255),
    PRIMARY KEY (staff_id)
    );

CREATE TABLE contract_table (
    contract_table_id int NOT NULL ,
    stimated_cost DECIMAL,
    completion_date int,
    PRIMARY KEY (contract_table_id)
    );


CREATE TABLE manager (
    manager_id int NOT NULL ,
    manager_name varchar(255) NOT NULL,
    manager_location varchar(255),
    PRIMARY KEY (manager_id)
    );


CREATE TABLE client (
    client_id int NOT NULL ,
    client_name varchar(255) NOT NULL,
    client_location varchar(255),
    manager_id int NOT NULL ,
    contract_id int NOT NULL ,
    staff_id int NOT NULL ,
    PRIMARY KEY (client_id),
    FOREIGN KEY (manager_id) REFERENCES manager(manager_id),
    FOREIGN KEY (contract_id) REFERENCES contract_table(contract_table_id),
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id)
    );


    