CREATE TABLE prescription (
    prescription_id int NOT NULL ,
    drug varchar(255),
    prescription_date DATE,
    dosage varchar(255),
    PRIMARY KEY (prescription_id)
    );

CREATE TABLE patient (
    patient_id int NOT NULL ,
    patient_name varchar(255) NOT NULL,
    patient_DOB DATE,
    patient_address varchar(255),
    PRIMARY KEY (patient_id)
    );


CREATE TABLE doctor (
    doctor_name varchar(255),
    secretary_name  varchar(255),
    prescription_id int NOT NULL ,
    patient_id int NOT NULL ,
    PRIMARY KEY (doctor_name),
    FOREIGN KEY (prescription_id) REFERENCES prescription(prescription_id),
    FOREIGN KEY (patient_id) REFERENCES patient(patient_id)
    );
