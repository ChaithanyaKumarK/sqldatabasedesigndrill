CREATE TABLE prescription (
   prescription_id int NOT NULL ,
    drug varchar(255),
    prescription_date DATE,
    dosage varchar(255),
    doctor varchar(255),
    secretary  varchar(255),
    PRIMARY KEY (prescription_id)
    );



CREATE TABLE patient (
    patient_id int NOT NULL ,
    patient_name varchar(255) NOT NULL,
    date_of_birth DATE,
    patient_address varchar(255),
    prescription_id int NOT NULL ,
    PRIMARY KEY (patient_id),
    FOREIGN KEY (prescription_id) REFERENCES prescription(prescription_id)
    );
